run:
	docker-compose up -d

stop:
	docker-compose stop

prepare-laravel:
	docker-compose down
	docker-compose up -d
	sudo chown ${USER}:${USER} -R master
	sudo chmod -R 777 master/bootstrap/cache
	sudo chmod -R 777 master/storage
	docker-compose exec master php artisan key:generate --ansi
	#docker-compose exec php composer require predis/predis
	docker-compose exec master php artisan --version