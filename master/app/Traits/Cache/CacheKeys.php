<?php

namespace App\Traits\Cache;

trait CacheKeys
{
    use CacheGenerator;

    public function generateCacheKey(...$args): string
    {
        return $this->generate('cache|key', $args);
    }

    public function generateBindCacheKey(...$args): string
    {
        return $this->generate('bind|cache|key', $args);
    }

    public function allCacheKey(): string
    {
        return 'all|cache|key';
    }

    public function allActiveCacheKey(): string
    {
        return 'all|active|cache|key';
    }
}
