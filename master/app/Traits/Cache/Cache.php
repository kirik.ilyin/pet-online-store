<?php

namespace App\Traits\Cache;

trait Cache
{
    use CacheKeys, CacheTags {
        CacheKeys::generate insteadof CacheTags;
    }

    public function getDefaultCacheTime() {
        return 3600;
    }
}
