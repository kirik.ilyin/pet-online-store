<?php

namespace App\Traits\Cache;

use Illuminate\Cache\RedisStore;
use Illuminate\Support\Facades\Cache;

trait SetCachePrefix
{
    protected function setCachePrefix(?string $prefix = null)
    {
        if (Cache::getStore() instanceof RedisStore) {
            isset($prefix) ? Cache::getStore()->setPrefix($prefix) : Cache::getStore()->setPrefix(env('CACHE_PREFIX'));
        }
    }
}
