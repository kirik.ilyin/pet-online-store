<?php

namespace App\Traits\Cache;

trait CacheTags
{
    public function getExampleCacheTag(): string
    {
        return 'example';
    }

    public function getExampleTeasersCacheTag(): string
    {
        return 'example|teasers';
    }
}
