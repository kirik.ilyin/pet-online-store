<?php

namespace App\Traits\Cache;

trait CacheGenerator
{
    protected function generate($prefix, $args = null): string
    {
        return $prefix . ($args ? '|' . md5(serialize($args)) : '');
    }
}
