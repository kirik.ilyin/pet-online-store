<?php

namespace App\Console\Commands;

use App\Models\Product;
use Elasticsearch\Client;
use Illuminate\Console\Command;

class ReindexProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reindex:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexing (Create index, mapping, delete index if exist, indexing all products) Product in ElasticSearch';

    private $product;
    private $elasticsearch;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $elasticsearch, Product $product)
    {
        parent::__construct();

        $this->elasticsearch = $elasticsearch;
        $this->product = $product;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Indexing all products');

        try {
            $this->elasticsearch->indices()->delete(['index' => $this->product->getSearchIndex()]);
        } catch (\Exception $exception) {
            $this->info($exception);
        }

        $this->elasticsearch->indices()->create($this->settings($this->product));

        foreach (Product::cursor() as $product) {
            $this->elasticsearch->index([
                'index' => $product->getSearchIndex(),
                'type' => $product->getSearchType(),
                'id' => $product->getKey(),
                'body' => $product->toSearchArray(),
            ]);

            $this->output->write('.');
        }
        $this->info('\nDone!');
    }

    private function settings(Product $product)
    {
        return [
            'index' => $product->getSearchIndex(),
            'body' => [
                'settings' => [
                    'analysis' => [
                        'filter' => [
                            "russian_stop" => [
                                "type" => "stop",
                                "stopwords" => "_russian_"
                            ],
//                            "russian_keywords" => [
//                                "type" => "keyword_marker",
//                                "keywords" => ["пример"]
//                            ],
                            "shingle" => [
                                'type' => 'shingle',
                            ],
                            "mynGram" => [
                                'type' => 'edge_ngram',
                                'min_gram' => 3,
                                'max_gram' => 10,
                            ],
                            "russian_stemmer" => [
                                "type" => "stemmer",
                                "language" => "russian"
                            ],
                            'english_stemmer' => [
                                'type' => 'stemmer',
                                'language' => 'english',
                            ]
                        ],
                        'analyzer' => [
                            'rebuilt_russian' => [
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'russian_stop', 'russian_stemmer', 'mynGram', 'english_stemmer', 'trim']
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        'is_active' => ['type' => 'boolean'],
//                        'created_at' => ['type' => 'text'],
//                        'id' => ['type' => 'text'],// Убрать
                        'description' => [
                            'type' => 'text',
                            'analyzer' => 'rebuilt_russian',
                        ],
                        'name' => [
                            'type' => 'text',
                            'analyzer' => 'rebuilt_russian',
                        ],
                        'updated_at' => ['type' => 'keyword'],
                        'code' => ['type' => 'keyword'],
                        'brand_id' => ['type' => 'keyword'],
                        'category_id' => ['type' => 'keyword'],
                        'price' => ['type' => 'long'],
                    ]
                ]
            ]
        ];
    }
}
