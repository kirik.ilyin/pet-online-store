<?php

namespace App\Http\Controllers;

use App\Http\Resources\FilterProducts;
use App\Master\Contracts\Repositories\BrandRepository;
use App\Master\Contracts\Repositories\CategoryRepository;
use App\Master\Contracts\Services\ProductService;
use App\Master\Services\Product\ElasticSearchService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $service;

    public function __construct()
    {
        $this->service = app(ProductService::class);
    }

    public function index(ElasticSearchService $serviceElastic)
    {
        $products = $this->service->paginate(8);

        $filters = $serviceElastic->getAllAggregations();

        //todo: Рефактор, лучше вынести в метод и учичесть request
        return Inertia::render('Product/products',
            (new FilterProducts(
            collect([
                'products' => $products,
                'brands' => $filters->get('brands'),
                'categories' => $filters->get('categories')
            ])
        ))->toArray(request()));
    }

    public function show(Request $request, string $code)
    {
        $product = $this->service->getByCode($code);
        return Inertia::render('Product/detail', [
            'product' => $product,
        ]);
    }

    public function search(Request $request) // todo: мб вынести в отдельный контроллер
    {
        $products = $this->service->search($request)->paginate(9);
        dd($products);
    }
}
