<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TestController extends Controller
{
    public function index()
    {        ///storage/preview_images/EcHlmfEq25CvorguMnz62MnhLDAMkfXDyHQ3aRlG.jpg
        $brand = Brand::all();
        $pictUrlTest = [
            '/pict/brand/685.gif',
            '/pict/brand/1077.gif',
            '/pict/brand/4488.gif',
        ];
        $brand->map(function ($brand) use ($pictUrlTest) {
            $brand->images()->create([
                'url' => Arr::random($pictUrlTest)
            ]);
        });
//        $brand->images()->create([
//            'url' => '/pict/brand/685.gif'
//        ]);
        dd(collect($brand));
    }
}
