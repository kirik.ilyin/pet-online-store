<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Inertia\Inertia;
use Illuminate\Http\JsonResponse;

class BasketController extends Controller
{
    public function index()
    {
        return Inertia::render('Basket/index');
    }

    public function add(string $id, Request $request): JsonResponse
    {
        $basket = $this->getBasket();

        $quantity = $request->input('quantity') ?? 1;

        if($basket && $basket->products->contains($id)){
            $pivotRow = $basket->products()->where('product_id', $id)->first()->pivot;

            $pivotRow->update(['quantity' => $quantity]);
        } else {
            $basket->products()->attach($id, ['quantity' => $quantity]);
        }

        return response()->json(['success' => true, 'quantity' => $quantity]);
    }

    public function check(string $id, Request $request)
    {
        $basket = $this->getBasket();

        if($basket)
        {
            $quantity = $basket->products()->where('product_id', $id)->first()->pivot->quantity ?? null;
        }

        return response()->json(['success' => true, 'quantity' => $quantity ?? null]);
    }

    public function remove(string $id, Request $request): JsonResponse
    {
        $basket = $this->getBasket();
        if($basket){
            $basket->products()->detach($id);
            $success = true;
        }

        return response()->json(['success' => $success ?? false]);
    }

    private function getBasket() :?Model
    {
        if(Auth::user() !== null){
            if(Auth::user()->basket()->get()->isEmpty()){
                $basket = Auth::user()->basket()->create();
            } else {
                $basket = Auth::user()->basket;
                $basket->touch();
            }
        } elseif ($basket_id = request()->cookie('basket_id')){
            // корзина уже существует, получаем объект корзины
            //todo: Доделать логику, если в куки не существующая корзина то создать + записать новое значение в куки
            $basket = Basket::findOrNew($basket_id);
            Cookie::queue('basket_id', $basket->id, 2000);
            // обновляем поле `updated_at` таблицы `baskets`
            $basket->touch();
        }
        else {
            $basket = Basket::create();
            Cookie::queue('basket_id', $basket->id, 2000);
        }

        return $basket;
    }
}
