<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Master\Contracts\Services\Product\ElasticSearchService;
use App\Master\Contracts\Services\ProductService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SearchController extends Controller
{
    /**
     * @var ElasticSearchService
     */
    private ElasticSearchService $service;

    public function __construct()
    {
        $this->service = app(ElasticSearchService::class);
    }

    public function search(Request $request)
    {
        $products = $this->service->search($request);
        dd($products);
    }
}
// Paginate With elastic like
//$per_page = $request->get('limit', 10);
//$from = ($request->get('page', 1) - 1) * $per_page;
//$access = $this->repository->Index($per_page, $from);
//
//$admin_exceptions = new LengthAwarePaginator(
//    $access['hits'],
//    $access['total'],
//    $per_page,
//    Paginator::resolveCurrentPage(),
//    ['path' => Paginator::resolveCurrentPath()]);
