<?php

namespace App\Http\Resources;

use App\Master\Contracts\Repositories\BrandRepository;
use App\Master\Contracts\Repositories\CategoryRepository;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class FilterProducts extends JsonResource
{

    private $categories;
    private $brands;

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->categories = collect($resource->get('categories'));
        $this->brands = collect($resource->get('brands'));
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'products' => $this->resource['products'],
            'brands' => $this->prepareBrands($this->brands),
            'categories' => $this->prepareCategories($this->categories),
        ];
    }

    private function prepareBrands(Collection $brands)
    {
        $allBrands = app(BrandRepository::class)->getAllActive()->keyBy('id');

        return $brands->map(function ($brand) use ($allBrands) {
            return array_merge($brand, $allBrands[$brand['key']]->toArray());
        });
    }

    private function prepareCategories(Collection $brands)
    {
        $allCategories = app(CategoryRepository::class)->getAllActive()->keyBy('id');

        return $brands->map(function ($category) use ($allCategories) {
            return array_merge($category, $allCategories[$category['key']]->toArray());
        });
    }
}
