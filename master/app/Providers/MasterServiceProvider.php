<?php

namespace App\Providers;

use App\Master\Repositories\BrandRepository;
use App\Master\Repositories\CategoryRepository;
use App\Master\Services;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use App\Master\Contracts;
use App\Master\Repositories\ProductRepository;
use App\Models\Product;

class MasterServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
        $this->registerServices();
    }

    private function registerRepositories()
    {
        $this->app->bind(Contracts\Repositories\ProductRepository::class, function () {
            return new ProductRepository(new Product());
        });
        $this->app->bind(Contracts\Repositories\CategoryRepository::class, function () {
            return new CategoryRepository(new Category());
        });
        $this->app->bind(Contracts\Repositories\BrandRepository::class, function () {
            return new BrandRepository(new Brand());
        });


//        $this->app->bind(Contracts\Repositories\ProductRepository::class, function () {
            // Это полезно, если мы хотим выключить наш кластер
            // или при развертывании поиска на продакшене
//            if (! config('services.search.enabled')) {
//                return new PostRepository(new Posts());
//            }
//            return new ElasticSearchRepository(
//                new Posts(),
//                $this->app->make(Client::class)
//            );
//        });

//        $this->bindSearchClient();
    }

//    private function bindSearchClient()
//    {
//        $this->app->bind(Client::class, function ($app) {
//            return ClientBuilder::create()
//                ->setHosts($app['config']->get('services.search.hosts'))
//                ->build();
//        });
//    }

    private function registerServices()
    {
        $this->app->bind(Contracts\Services\ProductService::class, Services\ProductService::class);
        $this->app->bind(Contracts\Services\Product\ElasticSearchService::class, Services\Product\ElasticSearchService::class);
    }

    public function provides()
    {
        return [
            Contracts\Repositories\ProductRepository::class,
            Contracts\Repositories\BrandRepository::class,
            Contracts\Repositories\CategoryRepository::class,
            Contracts\Services\ProductService::class,
        ];
    }
}
