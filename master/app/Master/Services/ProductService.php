<?php

namespace App\Master\Services;

use App\Master\Contracts\Repositories\ProductRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use App\Master\Contracts\Services\ProductService as ProductContract;

class ProductService implements ProductContract
{
    public function getByCode(string $code): ?Model
    {
        return tap(app(ProductRepository::class)->getByCode($code), function ($product){
            app(ProductRepository::class)->bindCategory($product);
            app(ProductRepository::class)->bindBrand($product);
        });
    }

    public function paginate(int $limit = 5): ?LengthAwarePaginator
    {
        return tap(app(ProductRepository::class)->paginate($limit), function ($product){
            app(ProductRepository::class)->bindCategory($product);
            app(ProductRepository::class)->bindBrand($product);
        });
    }
}
