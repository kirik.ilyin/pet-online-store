<?php

namespace App\Master\Contracts\Repositories;

use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface ProductRepository
{
    public function findProductsById(array $ids): ?Collection;
    public function getByCode(string $code): ?Model;
    public function paginate(int $limit = 5): ?LengthAwarePaginator;
    public function bindCategory($dealerships);
    public function bindBrand($products);
//    public function getById(string $id): Product;
}
