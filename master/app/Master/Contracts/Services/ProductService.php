<?php

namespace App\Master\Contracts\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface ProductService
{
    public function getByCode(string $code): ?Model;
    public function paginate(int $limit = 5): ?LengthAwarePaginator;
}
