<?php

namespace App\Master\Contracts\Services\Product;

use Illuminate\Http\Request;

interface ElasticSearchService
{
    public function search(Request $request);
}
