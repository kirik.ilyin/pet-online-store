<?php

namespace App\Master\Repositories;

use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Master\Contracts\Repositories\ProductRepository as ProductContract;

class ProductRepository extends AbstractRepository implements ProductContract
{
    public function findProductsById(array $ids): ?Collection
    {
//todo : Сделать кэширование данных
//        $tag = $this->getProductsCacheTag();
//        $key = $this->generateCacheKey($ids);

//        return Cache::tags($tag)->remember($key, $this->getDefaultCacheTime(), function () use ($ids) {
            return $this->getModel()
                ->select(
                    'id',
                    'name',
                    'code',
                    'description',
                    'created_at',
                    'is_active',
                    'price',
                )
                ->whereIn('id', $ids)
                ->where('is_active', '=', true)
                ->get();
//        });
    }

    public function getByCode(string $code): ?Model
    {
        return $this->getModel()
            ->select(
                'id',
                'name',
                'code',
                'description',
                'created_at',
                'is_active',
                'price',
                'brand_id',
                'category_id',
            )
            ->where('code', $code)
            ->where('is_active', true)
            ->first();
    }

    public function paginate(int $limit = 5): ?LengthAwarePaginator
    {
        return $this->getModel()->paginate($limit);
    }

    public function bindCategory($products)
    {
        return empty($products) ? $products : $products->load('category');
    }

    public function bindBrand($products)
    {
        return empty($products) ? $products : $products->load('brand');
    }

    public function bindPhotos($dealerships)
    {
        return empty($dealerships) ? $dealerships : $dealerships->load('images');
    }

    public function bindExcellences($dealerships)
    {
        if (!empty($dealerships)) {
            $dealerships->load(
                ['excellences' => function ($query) {
                    /** @var QueryBuilder $query */
                    $query->select([
                        'id',
                        'title',
                        'description',
                        'icon_id',
                        'detail_icon_id',
                        'sort',
                    ])->orderBy('sort', 'desc');
                }]
            );

            if ($dealerships instanceof  Collection) {
                $dealerships = $this->sortByExcellencesSort($dealerships);
            }
        }
        return $dealerships;
    }

    public function create(array $properties): Model
    {
        DB::beginTransaction();

        $model = $this->getModel();

        $model->fill($properties);
        $model->save();

        if (!empty($properties['files'])) {
            $model->files()->sync($properties['files']);
        }

        DB::commit();

        return $model;
    }

    public function update(string $id, array $properties): bool
    {
        DB::beginTransaction();

        if (($model = $this->getById($id)) && $success = $model->update($properties)) {
            if (array_key_exists('files', $properties)) {
                $model->files()->detach();
                if (!empty($properties['files'])) {
                    $model->files()->sync($properties['files']);
                }
            }
        }

        DB::commit();

        return $success ?? false;
    }
}
