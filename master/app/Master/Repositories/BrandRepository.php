<?php

namespace App\Master\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Master\Contracts\Repositories\BrandRepository as BrandContract;

class BrandRepository extends AbstractRepository implements BrandContract
{
    public function getAllActive(): ?Collection
    {
        return $this->getModel()
            ->select(
                'id',
                'name',
                'code',
                'description',
                'created_at',
                'is_active',
            )
            ->where('is_active', true)
            ->get();
    }

    public function findByIds(array $ids): ?Collection
    {
        if (empty($ids)) {
            return $this->getCollection();
        }

        return $this->getModel()
            ->select(
                'id',
                'name',
                'code',
                'description',
                'created_at',
                'is_active',
            )
            ->where('is_active', true)
            ->find($ids);
    }
}
