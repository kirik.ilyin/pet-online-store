<?php

namespace App\Models;

use App\Traits\HasUuid;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, HasUuid, Searchable;

    protected $fillable = [
        'name',
        'code',
        'description',
        'created_at',
        'updated_at',
        'is_active',
        'brand_id',
        'category_id',
        'price',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
        'updated_at' => 'date:M j, Y, g:i a',
        'created_at' => 'date:M j, Y, g:i a',
    ]; //Конвертация значений, особенно удобно если нужно хранить данные в json формате, то в sql нужно создать поле text и класть туда просто массив невозможно и будет ошибка, для этого просто добавить поле и array, напр - 'options' => 'array'

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function brand(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    public function baskets() {
        return $this->belongsToMany(Basket::class)->withPivot('quantity');
    }

    public function toSearchArray() : array
    {
        // Наличие пользовательского метода
        // преобразования модели в поисковый массив
        // позволит нам настраивать данные
        // которые будут доступны для поиска
        // по каждой модели.
        return [
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'brand_id' => $this->brand->id,
            'category_id' => $this->category->id,
            'price' => $this->price,
        ];
    }
}
