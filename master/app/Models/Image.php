<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'imageable_type',
        'imageable_id',
        'url',
        'description',
        'sort',
        'created_at',
        'updated_at',
        'is_active',
    ];

    /**
     * Get the parent imageable model (user or post).
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
