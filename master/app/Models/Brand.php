<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'name',
        'code',
        'description',
        'created_at',
        'updated_at',
        'is_active',
    ];

    protected $with = ['image'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Get the parent imageable model (user or post).
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
