<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/test', [Controllers\TestController::class, 'index']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

// Админка
Route::group(['prefix' => 'dashboard','middleware' => 'auth:sanctum', 'verified'], function() {
    Route::get('/', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::resource('/roles', Controllers\Admin\RolesController::class);
    Route::resource('/users', Controllers\Admin\UsersController::class);
});

Route::group(['prefix' => 'products'], function () {
    Route::get('/', [Controllers\ProductController::class, 'index'])->name('products');
    Route::get('/search', [Controllers\Product\SearchController::class, 'search'])->name('products');
    Route::get('/filter', [Controllers\ProductController::class, 'filter'])->name('products.filter');
});
Route::group(['prefix' => 'product'], function () {
    Route::get('/{code}', [Controllers\ProductController::class, 'show']);
});

Route::group(['prefix' => 'basket'], function () {
    Route::get('/', [Controllers\BasketController::class, 'index'])->name('basket.index');
    Route::get('/check/{id}', [Controllers\BasketController::class, 'check'])->name('basket.check');
    Route::post('/add/{id}', [Controllers\BasketController::class, 'add'])->name('basket.add');
    Route::delete('/remove/{id}', [Controllers\BasketController::class, 'remove'])->name('basket.remove');
});
