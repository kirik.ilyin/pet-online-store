<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
//        $products = Product::select('id')->where('is_active', true)->get();
//        $categories = Category::select('id')->get();
//
//        $products->map(function (Model $product) use ($categories) {
//            $product->categories()->attach($categories->random());
//        });

        $name = implode(' ', $this->faker->unique()->words(2));
        $brandIds = Brand::select('id')->get();
        $categoryIds = Category::select('id')->get();

        return [
            'name' => Str::title($name),
            'code' => Str::snake($name),
            'description' => $this->faker->text(100),
            'price' => random_int(100, 100000) / 10,
            'brand_id' => $brandIds->random(),
            'category_id' => $categoryIds->random(),
        ];
    }
}
