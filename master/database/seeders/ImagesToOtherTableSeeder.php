<?php
//php artisan db:seed --class=CategoryTableSeeder
namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ImagesToOtherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand = Brand::all();
        $brandPictUrlTest = [
            '/pict/brand/685.gif',
            '/pict/brand/1077.gif',
            '/pict/brand/4488.gif',
        ];
        $brand->map(function ($brand) use ($brandPictUrlTest) {
            $brand->images()->create([
                'url' => Arr::random($brandPictUrlTest)
            ]);
        });
    }
}
