<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRolesAndRoleTable extends Migration
{
    private $tables = [
        'Roles',
        'UserRole',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "up{$table}")) {
                $this->{$method}();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "down{$table}")) {
                $this->{$method}();
            }
        }
    }

    private function upRoles()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 100);
            $table->string('code', 50);
            $table->timestamps();
        });
    }

    private function downRoles()
    {
        Schema::dropIfExists('roles');
    }

    private function upUserRole()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->foreignUuid('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignUuid('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    private function downUserRole()
    {
        Schema::dropIfExists('user_role');
    }
}
