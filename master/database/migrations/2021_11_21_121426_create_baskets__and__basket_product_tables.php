<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketsAndBasketProductTables extends Migration
{

    private $tables = [
        'Baskets',
        'BasketProduct',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "up{$table}")) {
                $this->{$method}();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "down{$table}")) {
                $this->{$method}();
            }
        }
    }

    private function upBaskets()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('user_id')->unique()->nullable();
            $table->timestamps();
        });
    }

    private function downBaskets()
    {
        Schema::dropIfExists('baskets');
    }

    private function upBasketProduct()
    {
        Schema::create('basket_product', function (Blueprint $table) {
            $table->foreignUuid('basket_id');
            $table->foreignUuid('product_id');
            $table->unsignedTinyInteger('quantity');
            $table->index(['basket_id', 'product_id']);
        });
    }

    private function downBasketProduct()
    {
        Schema::dropIfExists('basket_product');
    }
}
