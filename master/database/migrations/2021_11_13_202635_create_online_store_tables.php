<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineStoreTables extends Migration
{
    private $tables = [
        'Products',
        'Categories',
        'Properties',
        'ProductProperty',
        'Images',
        'Cities',
        'Stores',
        'ProductStoreCount',
        'Review',
        'Brands',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "up{$table}")) {
                $this->{$method}();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            if (method_exists($this, $method = "down{$table}")) {
                $this->{$method}();
            }
        }
    }

    private function upProducts()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 100);
            $table->string('code', 50)->unique();
            $table->text('description')->nullable();
            $table->uuid('brand_id')->nullable();
            $table->uuid('category_id')->nullable();
            $table->boolean('is_active')->default('true');
            $table->decimal('price', 10, 2, true)->nullable();
            $table->timestamps();
        });
    }

    private function downProducts()
    {
        Schema::dropIfExists('products');
    }

    private function upBrands()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 255);
            $table->string('description', 1000)->nullable();
            $table->string('code')->unique();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    private function downBrands()
    {
        Schema::dropIfExists('brands');
    }

//    private function upProductCategory() //todo: Добавить подкатегории, возможно отдельную таблицу групп(общие группы, например: клавиатура - категория будет Клавиутыры и мыши, а группы, Компьютеры и комплектующие...)
//    {
//        Schema::create('product_category', function (Blueprint $table) {
//            $table->uuid('product_id');
//            $table->uuid('category_id');
//        });
//    }
//
//    private function downProductCategory()
//    {
//        Schema::dropIfExists('product_category');
//    }

    private function upCategories() //todo: Добавить подкатегории, возможно отдельную таблицу групп(общие группы, например: клавиатура - категория будет Клавиутыры и мыши, а группы, Компьютеры и комплектующие...)
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 255);
            $table->string('description', 1000)->nullable();
            $table->string('code')->unique();
            $table->uuid('parent_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    private function downCategories()
    {
        Schema::dropIfExists('categories');
    }

    private function upProperties()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 255);
            $table->string('code',100);
        });
    }

    private function downProperties()
    {
        Schema::dropIfExists('properties');
    }

    private function upProductProperty()
    {
        Schema::create('product_property', function (Blueprint $table) {
            $table->uuid('product_id');
            $table->uuid('property_id');
            $table->string('value', 255);
        });
    }

    private function downProductProperty()
    {
        Schema::dropIfExists('product_property');
    }

    private function upImages()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->uuid('id')->primary();
//            $table->uuid('product_id');
            $table->nullableUuidMorphs('imageable');
            $table->string('url', 255);
            $table->string('description', 255)->nullable();
            $table->smallInteger('sort')->default(500);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    private function downImages()
    {
        Schema::dropIfExists('images');
    }

    private function upCities()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 100);
            $table->string('code', 20);
//            $table->double('lat')->nullable();
//            $table->double('lng')->nullable();
            $table->smallInteger('sort')->default(500);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    private function downCities()
    {
        Schema::drop('cities');
    }

    private function upStores()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('city_id');
            $table->string('name', 255);
            $table->string('address', 255);
            $table->string('phone', 20);
//            $table->double('lat', 100);
//            $table->double('lng', 100);
//            $table->smallInteger('rating');
            $table->string('work_time', 255);
            $table->smallInteger('sort')->default(500);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    private function downStores()
    {
        Schema::drop('stores');
    }

    private function upProductStoreCount()
    {
        Schema::create('product_store_count', function (Blueprint $table) {
            $table->uuid('product_id');
            $table->uuid('store_id');
            $table->integer('count');
        });
    }

    private function downProductStoreCount()
    {
        Schema::drop('product_store_count');
    }

    private function upReview() //todo: Добавить комментарии к отзывам
    {
        Schema::create('review', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->smallInteger('rating');
            $table->text('comment')->nullable();
            $table->boolean('recommendation')->default('true');
            $table->smallInteger('count_like')->nullable();
            $table->smallInteger('count_dislike')->nullable();
            $table->boolean('is_active')->default('true');
            $table->timestamp('date');
        });
    }

    private function downReview()
    {
        Schema::drop('review');
    }

}
